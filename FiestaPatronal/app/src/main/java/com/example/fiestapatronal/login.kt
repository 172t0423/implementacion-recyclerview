package com.example.fiestapatronal

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class login : AppCompatActivity() {
    var n = 1
    var i = 0
    var datos1 = arrayOfNulls<String>(n)
    var datos2 = arrayOfNulls<String>(n)
    var datos3 = arrayOfNulls<String>(n)
    var datos4 = arrayOfNulls<String>(n)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val bundle: Bundle? = intent.extras
        bundle?.let { bundleLibriDeNull ->
            val nombres = bundleLibriDeNull.getString("key_nombres", "")
            val apellidos = bundleLibriDeNull.getString("key_apellido", "")
            val us = bundleLibriDeNull.getString("key_us", "")
            val contra = bundleLibriDeNull.getString("key_contra1", "")
                datos1[i] = nombres.toString()
                datos2[i] = apellidos.toString()
                datos3[i] = us.toString()
                datos4[i] = contra.toString()
        }
        btn.setOnClickListener {
            val i = 0
            if (edtUser.text.toString().isEmpty()) {
                Toast.makeText(this, "Debe introducir un usuario", Toast.LENGTH_SHORT).show()
            }
            if (edtContrasena.text.toString().isEmpty()) {
                Toast.makeText(this, "Debe introducir una contraseña", Toast.LENGTH_SHORT).show()
            }
         if (datos3[i].equals(edtUser.text.toString()) && datos4[i].equals(edtContrasena.text.toString())) {
                    val i = Intent(Intent(this, Menu::class.java))
                    i.putExtra("USUARIO", edtUser.text.toString())
                    startActivity(i)
                           } else {
                    Toast.makeText(this, "Usuario no encontrado", Toast.LENGTH_SHORT).show()
                }

        }
        tvRegistrate.setOnClickListener {
            startActivity(Intent(this, registro::class.java))
        }

    }
}